import seaborn as sns; sns.set
import matplotlib.pyplot as plt
# import plotly.tools as tls
# import plotly.plotly as py
import pandas as pd

# Visualisations

# Setting the style
sns.set(style = "whitegrid")

# Read in the data
df = pd.read_csv("trust_edited.csv")

# Plots

# Bar plots
print("Read in CSV file, about to plot it...")
var_status = df['variation_status']
arr_dep = df['event_type']
hour_of_day = df['hour_of_the_day']
day_of_the_week = df['day_of_the_week']


val_count = var_status.value_counts()
print("\nVal count:\n{}".format(val_count))

print("Plotting")
# Plot of Frequency of Variation Status
sns.countplot(x=var_status, palette='Blues')
plt.xlabel("Variation Status")
plt.ylabel("Frequency")
plt.suptitle("Bar Plot of Frequency of Variation Status")
# plt.close()
#
# # Plot of Frequency of Arrivals vs Departures
# sns.countplot(x=arr_dep, palette='Blues')
# plt.xlabel("Event Type")
# plt.ylabel("Frequency")
# plt.title("Bar Plot of Frequency of Arrivals and Departures")
# plt.close()
#
#
# # Heat map
# print(df.head())
# df.pivot_table(index=arr_dep, columns=hour_of_day, values=val_count)

# matrix_trips = df.corr()
# sns.heatmap(matrix_trips)
# plt.close()
#
# # Heat map of total delays at locations and corresponding delay code (upward direction)
# sns.heatmap(matrix_trips, annot=False, cmap='Blues', linecolor='white', linewidths=0.5)
# plt.xlabel("Variation Status")
# plt.ylabel("Frequency")
# plt.title("Bar Plot of total delays at locations and corresponding delay code (upward direction)")
# plt.close()
#
# # Heat map of total delays at locations and corresponding delay code (downward direction)
# sns.heatmap(matrix_trips, annot=False, cmap='Blues', linecolor='white', linewidths=0.5)
# plt.xlabel("Variation Status")
# plt.ylabel("Frequency")
# plt.title("Heatmap of total delays at locations and corresponding delay code (downward direction)")
# plt.close()

plt.show()