import requests
import os

# Define which data do you wanna analyse ...
year = 2018
month = 6
from_date = 1
to_date = 1
typeofdata = 'trust'  # 'trust' or 'vstp'


log_file_dir = os.path.join(os.getcwd(), r'log_files')
if not os.path.exists(log_file_dir):
    os.makedirs(log_file_dir)
folder_path = os.path.join(log_file_dir + '/folder_list.txt')
downloaded_files = []

for day in range(from_date, to_date + 1):

    data_website = 'https://networkrail.opendata.opentraintimes.com/mirror'
    url = data_website + '/{}/{:4}/{:02}/{:02}/'.format(typeofdata, year, month, day)
    r = requests.get(url)

    print('url :', url)
    print('encoding :', r.encoding)
    print('Status code is Ok :', r.status_code == requests.codes.ok)
    print('content-type :', r.headers['content-type'])

    from lxml import html

    links = html.fromstring(r.content)
    folder_list = links.xpath('//a/@href')[7:10]
    downloaded_files.extend(folder_list)
    print(downloaded_files)

with open(folder_path, 'w') as f:
    for item in downloaded_files:
        f.write("{}\n" .format(item))

for i, name in enumerate(downloaded_files):
    response = requests.get(url + downloaded_files[i])
    with open(os.path.join(log_file_dir, name), "wb") as code:
        code.write(response.content)

