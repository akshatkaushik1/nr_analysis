import requests
import os
import shutil

# Define which data do you want to analyse ...
year = 2018
month = 6
from_date = 1
to_date = 3
typeofdata = 'trust'  # 'trust' or 'vstp'


log_file_dir = os.path.join(os.getcwd(), r'log_files')
if os.path.exists(log_file_dir): shutil.rmtree(r'log_files')
os.makedirs(log_file_dir)

folder_path = os.path.join(log_file_dir + '/folder_list.txt')
downloaded_files = []

for day in range(from_date, to_date + 1):

    data_website = 'https://networkrail.opendata.opentraintimes.com/mirror'
    url = data_website + '/{}/{:4}/{:02}/{:02}/'.format(typeofdata, year, month, day)
    r = requests.get(url)

    print('url :', url)
    print('encoding :', r.encoding)
    print('Status code is Ok :', r.status_code == requests.codes.ok)
    print('content-type :', r.headers['content-type'])

    from BeautifulSoup import BeautifulSoup

    soup = BeautifulSoup(r.content)
    for link in soup.findAll('a')[7:10]:
        item = link.get('href')
        print(item)

        response = requests.get(url + item)
        with open(os.path.join(log_file_dir, item), 'wb') as code:
             code.write(response.content)

        with open(folder_path, 'a') as f:
                f.write("{}\n".format(item))

