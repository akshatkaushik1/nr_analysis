import requests
import os

# Define which data do you wanna analyse ...
year = 2018
month = 6
from_date = 1
to_date = 1
typeofdata = 'trust' # 'trust' or 'vstp'

for day in range(from_date,to_date+1):
	
	data_website = 'https://networkrail.opendata.opentraintimes.com/mirror'
	url = data_website+'/{}/{:4}/{:02}/{:02}/'.format(typeofdata,year,month,day)
	r = requests.get(url)

	print 'url :', url
	print 'ecoding :', r.encoding
	print 'Status code is Ok :', r.status_code == requests.codes.ok
	print 'content-type :', r.headers['content-type']


	from lxml import html
	links = html.fromstring(r.content)
	folder_list = links.xpath('//a/@href')[7:10]


	for i, name in enumerate(folder_list):
		print name

		response = requests.get(url+folder_list[i])  
		with open(name, "wb") as code:
			code.write(response.content)

