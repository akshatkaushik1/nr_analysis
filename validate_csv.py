import pandas as pd
from datetime import datetime
from dateutil import parser
import numpy as np


# Further editing once csv has been made
df = pd.read_csv("original_trust.csv")

# Fixing NAs
df['loc_stanox'] = df['loc_stanox'].fillna(0.0)
df['loc_stanox'] = df['loc_stanox'].astype(int)
df['actual_timestamp'] = df['actual_timestamp'].fillna(1527814800000)
df['actual_timestamp'] = df['actual_timestamp'].astype(int)


# Changing actual_timestamp to more interpretable info (date, day, hour)
ts = df['actual_timestamp']

proper_ts = []
date =[]
day = []
hour =[]

for m in range(len(ts)):
    proper_ts.append(ts[m]/1000)  # Removing last 3 digits of timestamp to allow conversion
    date.append(datetime.utcfromtimestamp(proper_ts[m]).strftime('%Y-%m-%d %H:%M:%S'))  # Convert timestamp to date
    hour.append(datetime.utcfromtimestamp(proper_ts[m]).strftime('%H'))  # Convert timestamp to hour of the day

# Needs a seperate loop since dependant on 'date'
for n in range(len(ts)):
    day.append(parser.parse(date[n]).strftime("%w"))  # Convert date to day of the week

# Adding columns populated by zeros
df['shortened_ts'] = proper_ts
df['timestamp_as_date'] = date
df['day_of_the_week'] = day
df['hour_of_the_day'] = hour
del df['Unnamed: 0']

df.to_csv('trust_edited.csv')  # Re-writing CSV with edited changes


# # Merging darwin and trust data
# trust = pd.read_csv("original_trust.csv")
# darwin = pd.read_csv("darwin.csv")
# merged = trust.merge(darwin, on='')
# merged.to_csv("darwin_trust.csv", index=False)
