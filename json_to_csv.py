import gzip
import json
import pandas as pd
import os


# Open text file from requestdata.py that contains list of files from NR
current_dir = os.getcwd()
log_file_dir = os.path.join(current_dir, r'log_files')
folder_path = os.path.join(log_file_dir + '/folder_list.txt')

with open(folder_path) as f:
    log_files = f.read().splitlines()
print(log_files)


test = ['trust-201806100000.log.gz', 'trust-201806100001.log.gz', 'trust-201806100002.log.gz']#, 'trust-201806110000.log.gz', 'trust-201806110001.log.gz', 'trust-201806110002.log.gz']
print(test)

trips = []

# For each file (i.e. each second per day)
for file in test:
    with gzip.open('./log_files/'+file, 'rb') as f: #os.path.join(log_file_dir, file)
        # Splitting the data as it was one line
        # Have multiple train info for each second
        print('{}'.format(file))
        lines = [x.decode('utf8').strip() for x in f.readlines()]
        lines[0] = lines[0].replace('[','')
        lines[0] = lines[0].replace(']','')
        lines[0] = lines[0].replace('"', '')
        lines[0] = lines[0].replace('}{\"header\"','},{\"header\"')
        journeys_every_sec = lines[0].replace('}},','}}*').split('*')

    # Empty list that we will dump the JSON data into
    trust = []

    # For each train journey per sec
    for i in range(len(journeys_every_sec)):
        # Empty list for each train journey data
        header_keys = []
        header_values = []
        body_keys = []
        body_values = []

        # Splitting data into keys and values for:
        # Header
        journey_i_header = journeys_every_sec[i].split('},body')[0]
        journey_i_header_data = journey_i_header.split('header:{')[1]
        journey_i_header_data_split = journey_i_header_data.split(',')

        # Body
        journey_i_body = journeys_every_sec[i].split('},body:{')[1]
        journey_i_body_data = journey_i_body.split('}}')[0]
        journey_i_body_data_split = journey_i_body_data.split(',')

        # Adding header keys and values to lists
        for j in range(len(journey_i_header_data_split)):
            journey_i_header_data_keys_values = journey_i_header_data_split[j].split(':')
            header_keys.append(journey_i_header_data_keys_values[0])
            header_values.append(journey_i_header_data_keys_values[1])

        # Adding body keys and values to lists
        for k in range(len(journey_i_body_data_split)):
            journey_i_body_data_keys_values = journey_i_body_data_split[k].split(':')
            body_keys.append(journey_i_body_data_keys_values[0])
            body_values.append(journey_i_body_data_keys_values[1])

        # Creating a dictionary with header and body keys and values
        data_dict_header = dict(zip(header_keys, header_values))
        data_dict_body = dict(zip(body_keys, body_values))

        # Combining header and body dictionaries
        data_dict_journey_i = {**data_dict_header, **data_dict_body}

        # Adding to list
        trust.append(data_dict_journey_i)

    trips.extend(trust)

    # Dump list into JSON file
    with open('original_trust.json', 'w') as f:
        f.write(json.dumps(trips))

    # Convert to CSV
    df = pd.read_json("original_trust.json", convert_dates=False)
    df.to_csv('original_trust.csv')
